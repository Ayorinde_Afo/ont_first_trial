library(tidyverse)
library(Rmisc)
library(ggthemes)

# I filtered because I wanted to select only barcodes common to both the 1st and the 2nd run. There were only 7 of such
all_summarized_dfs_edited_plot <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", "barcode", 'Species', 'ST_sign', 'Timepoint')) %>% filter(N > 1) %>% 
  ggplot(aes(x=factor(Timepoint, levels = c("24K", "44K", "64K", "84K", "104K")), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  # ggplot(aes(x=factor(correction, levels = c('completely uncorrected', 'error-corrected before strainberry', 'error-corrected after strainberry', 'error-corrected before and after strainberry')), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 3, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_wrap(Species ~ factor(ST_sign, levels = c('50:50*', '40:60*', '40:60**', '30:70**', '20:30:50***', '10:25:65***')), nrow = 2) +
  theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 20, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 15, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))

# save
ggsave("~/analysis/ONT_data/cgmlst/first_second_and_third_run/figures/error_bars_duplicates_run_25102021_and_09022022_intact.jpeg", all_summarized_dfs_edited_plot, device = "jpeg", width = 10000, height = 6000, units = 'px', dpi = 600)

# try plotting all possible values
Timepoint_level_all_1st_2_runs <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", "barcode", 'Species', 'ST_sign', 'Timepoint')) %>% distinct(Timepoint) %>% pull() %>% stringr::str_sort(numeric = TRUE)
## because somehow the above code 'thinks' that '144.8K' is higher than '144K', I need to do this manually
Timepoint_level_all_1st_2_runs <- c("24K", "38K", "44K", "60.5K", "64K", "84K", "87K", "93K", "104K", "114K", "120.6K", "121.6K", "122.1K", "124K", "144K", "144.8K", "146.3K", "164K", "184K", "200K", "204K", "224K", "228K", "244K", "264K", "284K", "304K", "312K")

# plot
# I filtered because I wanted to select only barcodes common to both the 1st and the 2nd run. There were only 7 of such
all_summarized_dfs_edited_full_plot <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", "barcode", 'Species', 'ST_sign', 'Timepoint')) %>%  
  ggplot(aes(x= factor(Timepoint, levels = Timepoint_level_all_1st_2_runs), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 3, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_wrap(Species ~ factor(ST_sign, levels = c('50:50*', '40:60*', "20:30:50*", '40:60**', '30:70**', '20:30:50***', '10:25:65***')), nrow = 2, scales = 'free_x') + theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 20, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 15, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))

# save
ggsave("~/analysis/ONT_data/cgmlst/first_second_and_third_run/figures/error_bars_duplicates_run_25102021_and_09022022_intact_full.jpeg", all_summarized_dfs_edited_full_plot, device = "jpeg", width = 14000, height = 6000, units = 'px', dpi = 600)

########################################
# alternatively, plot Ecoli and Kleb plots differently, then join them
# I filtered because I wanted to select only barcodes common to both the 1st and the 2nd run. There were only 7 of such
all_summarized_dfs_edited_kpn <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", "barcode", 'Species', 'ST_sign', 'Timepoint')) %>% filter(N > 1) %>% 
  filter(Species == 'Klebsiella pneumoniae') %>% 
  ggplot(aes(x=factor(Timepoint, levels = c("24K", "44K", "64K", "84K", "104K")), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  # ggplot(aes(x=factor(correction, levels = c('completely uncorrected', 'error-corrected before strainberry', 'error-corrected after strainberry', 'error-corrected before and after strainberry')), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 3, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_grid(Species ~ factor(ST_sign, levels = c('50:50*', '40:60*', '40:60**', '30:70**', '20:30:50***', '10:25:65***')), scales = 'free_x') +
  theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 20, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 15, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))
# Ecoli
# I filtered because I wanted to select only barcodes common to both the 1st and the 2nd run. There were only 7 of such
all_summarized_dfs_edited_eco <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", "barcode", 'Species', 'ST_sign', 'Timepoint')) %>% filter(N > 1) %>% 
  filter(Species == 'Escherichia coli') %>% 
  ggplot(aes(x=factor(Timepoint, levels = c("24K", "44K", "64K", "84K", "104K")), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  # ggplot(aes(x=factor(correction, levels = c('completely uncorrected', 'error-corrected before strainberry', 'error-corrected after strainberry', 'error-corrected before and after strainberry')), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 3, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_grid(Species ~ factor(ST_sign, levels = c('50:50*', '40:60*', '40:60**', '30:70**', '20:30:50***', '10:25:65***')), scales = 'free_x') +
  theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 20, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 15, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))

# join both plots together
# https://stackoverflow.com/questions/64757410/shared-x-and-y-axis-labels-ggplot2-with-ggarrange
# windowsFonts() for font family check
require(grid)
all_summarized_dfs_edited_eco_kpn_duplicate_plot <- ggpubr::ggarrange(all_summarized_dfs_edited_kpn + ggpubr::rremove('ylab'), all_summarized_dfs_edited_eco + ggpubr::rremove('ylab'), common.legend = TRUE, ncol = 1, legend = 'right') %>% 
  ggpubr::annotate_figure(left = ggpubr::text_grob("allele proportion (%)", rot = 90, size = 30, face = 'bold', hjust = 0.25))

#save
ggsave("~/analysis/ONT_data/cgmlst/first_second_and_third_run/figures/error_bars_duplicates_run_25102021_and_09022022.jpeg", all_summarized_dfs_edited_eco_kpn_duplicate_plot, device = "jpeg", width = 10000, height = 6000, units = 'px', dpi = 600)


# Group the sub-barcode duplicates based on the number of STs
# I filtered because I wanted to select only barcodes common to both the 1st and the 2nd run. There were only 7 of such
# I used the commented code to determine my case_when parameters
# all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
# filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% distinct(Species, barcode, ST_sign)
all_summarized_dfs_edited_by_ST <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  dplyr::mutate(number_of_STs = case_when(barcode == 'G-Eco04714_Eco04749' & Species == 'Escherichia coli' & ST_sign == '50:50*' ~ 'one_ST',
                                          barcode == 'G-Kpn01528_Kpn01886' & Species == 'Klebsiella pneumoniae' & ST_sign == '40:60**' ~ 'two_STs',
                                          barcode == 'G-Eco04708_Eco04812' & Species == 'Escherichia coli' & ST_sign == '40:60*' ~ 'one_ST',
                                          barcode == 'G-16902_13733' & Species == 'Klebsiella pneumoniae' & ST_sign == '30:70**' ~ 'two_STs',
                                          barcode == 'G-15240_16126_5925' & Species == 'Klebsiella pneumoniae' & ST_sign == '20:30:50***' ~ 'three_STs',
                                          barcode == 'G-Eco04720_Eco04711_Eco04831' & Species == 'Escherichia coli' & ST_sign == '20:30:50*' ~ 'one_ST',
                                          barcode == 'G-13723_17053_12926' & Species == 'Klebsiella pneumoniae' & ST_sign == '10:25:65***' ~ 'three_STs',
                                          barcode == 'G-Eco04720_Eco04711_Eco04831' & Species == 'Escherichia coli' & ST_sign == '20:30:50***' ~ 'three_STs')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", 'Species', 'Timepoint', 'number_of_STs'))

# plot
all_summarized_dfs_edited_by_ST_plot <- all_summarized_dfs_edited_by_ST %>% 
  ggplot(aes(x= factor(Timepoint, levels = Timepoint_level_all_1st_2_runs), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 3, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_wrap(Species ~  factor(number_of_STs, levels = c('one_ST', 'two_STs', 'three_STs')), nrow = 2, scales = 'free_x') + theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 20, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 15, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))

#save
ggsave("~/analysis/ONT_data/cgmlst/first_second_and_third_run/figures/error_bars_duplicates_run_25102021_and_09022022_numberofST.jpeg", all_summarized_dfs_edited_by_ST_plot, device = "jpeg", width = 10000, height = 6000, units = 'px', dpi = 600)

### only duplicate datapoints will be selected
all_summarized_dfs_edited_by_ST_duplicate_datapoints <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  dplyr::mutate(number_of_STs = case_when(barcode == 'G-Eco04714_Eco04749' & Species == 'Escherichia coli' & ST_sign == '50:50*' ~ 'one_ST',
                                          barcode == 'G-Kpn01528_Kpn01886' & Species == 'Klebsiella pneumoniae' & ST_sign == '40:60**' ~ 'two_STs',
                                          barcode == 'G-Eco04708_Eco04812' & Species == 'Escherichia coli' & ST_sign == '40:60*' ~ 'one_ST',
                                          barcode == 'G-16902_13733' & Species == 'Klebsiella pneumoniae' & ST_sign == '30:70**' ~ 'two_STs',
                                          barcode == 'G-15240_16126_5925' & Species == 'Klebsiella pneumoniae' & ST_sign == '20:30:50***' ~ 'three_STs',
                                          barcode == 'G-Eco04720_Eco04711_Eco04831' & Species == 'Escherichia coli' & ST_sign == '20:30:50*' ~ 'one_ST',
                                          barcode == 'G-13723_17053_12926' & Species == 'Klebsiella pneumoniae' & ST_sign == '10:25:65***' ~ 'three_STs',
                                          barcode == 'G-Eco04720_Eco04711_Eco04831' & Species == 'Escherichia coli' & ST_sign == '20:30:50***' ~ 'three_STs')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", 'Species', 'Timepoint', 'number_of_STs')) %>% filter(N > 1)

# plot

all_summarized_dfs_edited_by_ST_duplicate_datapoints_plot <- all_summarized_dfs_edited_by_ST_duplicate_datapoints %>% 
  ggplot(aes(x= factor(Timepoint, levels = Timepoint_level_all_1st_2_runs), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 3, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_wrap(Species ~  factor(number_of_STs, levels = c('one_ST', 'two_STs', 'three_STs')), ncol = 3, scales = 'free_x') + theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 20, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 15, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))

#save
ggsave("~/analysis/ONT_data/cgmlst/first_second_and_third_run/figures/error_bars_run_25102021_and_09022022_numberofST_duplicate_datapoints.jpeg", all_summarized_dfs_edited_by_ST_duplicate_datapoints_plot, device = "jpeg", width = 10000, height = 4000, units = 'px', dpi = 600)

all_summarized_dfs_edited_by_ST_duplicate_datapoints$species_labels <- case_when(all_summarized_dfs_edited_by_ST_duplicate_datapoints$Species == 'Escherichia coli' & all_summarized_dfs_edited_by_ST_duplicate_datapoints$number_of_STs == 'one_ST' ~ str_replace(paste0("bolditalic(", all_summarized_dfs_edited_by_ST_duplicate_datapoints$Species,  ")", " ",  "(", "one_ST", ")"), " ", "~"),
                                                                                 all_summarized_dfs_edited_by_ST_duplicate_datapoints$Species == 'Klebsiella pneumoniae' & all_summarized_dfs_edited_by_ST_duplicate_datapoints$number_of_STs == 'two_STs' ~ str_replace(paste0("bolditalic(", all_summarized_dfs_edited_by_ST_duplicate_datapoints$Species,  ")", " ", "(", "two_STs", ")"), " ", "~"),
                                                                                 all_summarized_dfs_edited_by_ST_duplicate_datapoints$Species == 'Klebsiella pneumoniae' & all_summarized_dfs_edited_by_ST_duplicate_datapoints$number_of_STs == 'three_STs' ~ str_replace(paste0("bolditalic(", all_summarized_dfs_edited_by_ST_duplicate_datapoints$Species,  ")", " ", "(", "three_STs", ")"), " ", "~"))


# plot without error bars

all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot <- all_summarized_dfs_edited_by_ST_duplicate_datapoints %>% 
  ggplot(aes(x= factor(Timepoint, levels = Timepoint_level_all_1st_2_runs), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  # geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 4, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_wrap(Species ~  factor(number_of_STs, levels = c('one_ST', 'two_STs', 'three_STs')), ncol = 3, scales = 'free_x') + theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 25, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 25, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))

#save Q:/IUK-A-MIGE/PROJECTS/AFOLAYAN/Benchmarking/figures/
# ggsave("~/analysis/ONT_data/cgmlst/first_second_and_third_run/figures/error_bars_run_25102021_and_09022022_numberofST_duplicate_datapoints.jpeg", all_summarized_dfs_edited_by_ST_duplicate_datapoints_plot, device = "jpeg", width = 10000, height = 4000, units = 'px', dpi = 600)
# ggsave("~/Freiburg_lab/ONT/cgmlst/figures/all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot.jpeg", all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot, device = "jpeg", width = 10000, height = 6000, units = 'px', dpi = 600)
ggsave("Q:/IUK-A-MIGE/PROJECTS/AFOLAYAN/Benchmarking/figures/all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot.jpeg", all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot, device = "jpeg", width = 11000, height = 6000, units = 'px', dpi = 600)


# based on Stefany's suggestion on Italicize

all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot_italics <- all_summarized_dfs_edited_by_ST_duplicate_datapoints %>% 
  ggplot(aes(x= factor(Timepoint, levels = Timepoint_level_all_1st_2_runs), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  # geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 4, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  # facet_wrap(Species ~  factor(number_of_STs, levels = c('one_ST', 'two_STs', 'three_STs')), ncol = 3, scales = 'free_x') + theme_bw() + 
  facet_wrap(factor(species_labels, levels = c('bolditalic(Escherichia~coli) (one_ST)', 'bolditalic(Klebsiella~pneumoniae) (two_STs)', 'bolditalic(Klebsiella~pneumoniae) (three_STs)')) ~ ., labeller=label_parsed, ncol = 3, scales = 'free_x') +
  theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 30, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 25, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))


ggsave("Q:/IUK-A-MIGE/PROJECTS/AFOLAYAN/Benchmarking/figures/all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_italics.jpeg", all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot_italics, device = "jpeg", width = 16000, height = 8000, units = 'px', dpi = 600)

##############################
# Based on the number of strains that make up the microbial community regardless of the sequence type or sequence run

all_summarized_dfs_edited_by_number_of_strains <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  dplyr::mutate(number_of_strains = case_when(barcode == 'G-Eco04714_Eco04749' & Species == 'Escherichia coli' & ST_sign == '50:50*' ~ 'two strains',
                                              barcode == 'G-Kpn01528_Kpn01886' & Species == 'Klebsiella pneumoniae' & ST_sign == '40:60**' ~ 'two strains',
                                              barcode == 'G-Eco04708_Eco04812' & Species == 'Escherichia coli' & ST_sign == '40:60*' ~ 'two strains',
                                              barcode == 'G-16902_13733' & Species == 'Klebsiella pneumoniae' & ST_sign == '30:70**' ~ 'two strains',
                                              barcode == 'G-15240_16126_5925' & Species == 'Klebsiella pneumoniae' & ST_sign == '20:30:50***' ~ 'three strains',
                                              barcode == 'G-Eco04720_Eco04711_Eco04831' & Species == 'Escherichia coli' & ST_sign == '20:30:50*' ~ 'three strains',
                                              barcode == 'G-13723_17053_12926' & Species == 'Klebsiella pneumoniae' & ST_sign == '10:25:65***' ~ 'three strains',
                                              barcode == 'G-Eco04720_Eco04711_Eco04831' & Species == 'Escherichia coli' & ST_sign == '20:30:50***' ~ 'three strains')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", 'Species', 'Timepoint', 'number_of_strains'))

# plot
all_summarized_dfs_edited_by_number_of_strains_plot <- all_summarized_dfs_edited_by_number_of_strains %>% 
  ggplot(aes(x= factor(Timepoint, levels = Timepoint_level_all_1st_2_runs), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 3, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_wrap(Species ~  factor(number_of_strains, levels = c('two strains', 'three strains')), nrow = 2, scales = 'free_x') + theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 20, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 15, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))

#save
ggsave("~/analysis/ONT_data/cgmlst/first_second_and_third_run/figures/all_summarized_dfs_edited_by_number_of_strains_plot.jpeg", all_summarized_dfs_edited_by_number_of_strains_plot, device = "jpeg", width = 10000, height = 6000, units = 'px', dpi = 600)


# I need to replot by not excluding any metagenome. see what you did in 'summarize_plots.R'

### I would like to see all points even if they are represented only once
all_summarized_dfs_edited_by_ST_duplicate_datapoints_v2 <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  # filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  dplyr::mutate(number_of_STs = case_when(ST_sign == '50:50*' ~ 'one_ST',
                                          ST_sign == '50:50**' ~ 'two_STs',
                                          ST_sign == '40:60**' ~ 'two_STs',
                                          ST_sign == '40:60*' ~ 'one_ST',
                                          ST_sign == '30:70**' ~ 'two_STs',
                                          ST_sign == '30:70*' ~ 'one_ST',
                                          ST_sign == '20:30:50***' ~ 'three_STs',
                                          ST_sign == '20:30:50*' ~ 'one_ST',
                                          ST_sign == '10:25:65***' ~ 'three_STs',
                                          ST_sign == '10:25:65*' ~ 'one_ST')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", 'Species', 'Timepoint', 'number_of_STs'))

Timepoint_level_all_1st_2_runs_v2 <- all_summarized_dfs_edited %>% bind_rows(G_bind_subbarcodes_09022022) %>% 
  # filter(barcode %in% c('G-Kpn01528_Kpn01886', 'G-16902_13733', 'G-15240_16126_5925', 'G-13723_17053_12926', 'G-Eco04714_Eco04749', 'G-Eco04708_Eco04812', 'G-Eco04720_Eco04711_Eco04831')) %>% 
  summarySE(measurevar="allele_proportion", groupvars=c("double_single_correctness", "barcode", 'Species', 'ST_sign', 'Timepoint')) %>% distinct(Timepoint) %>% pull() %>% stringr::str_sort(numeric = TRUE)
## because somehow the above code 'thinks' that '144.8K' is higher than '144K', I need to do this manually
Timepoint_level_all_1st_2_runs_v2 <- c("24K", "38K", "44K", "60.5K", "63.5K", "64K", "76.8K", "84K", "87K", "93K", "104K", "114K", "120.6K", "121.6K", "122.1K", "124K", "144K", "144.8K", "146.3K", "164K", "184K", "189K", "200K", "204K", "211K", "216K", "224K", "228K", "244K", "250K", "264K", "284K", "304K", "312K")


# plot without error bars

all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot_v2 <- all_summarized_dfs_edited_by_ST_duplicate_datapoints_v2 %>% 
  ggplot(aes(x= factor(Timepoint, levels = Timepoint_level_all_1st_2_runs_v2), y=allele_proportion, group = double_single_correctness, colour=double_single_correctness)) + 
  # geom_errorbar(aes(ymin=allele_proportion-se, ymax=allele_proportion+se), width=.2, colour = 'black', position = position_dodge(0.1)) +
  geom_line(position = position_dodge(0.1)) +
  geom_point(size = 3, position = position_dodge(0.1)) +
  scale_colour_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  facet_wrap(Species ~  factor(number_of_STs, levels = c('one_ST', 'two_STs', 'three_STs')), ncol = 2, scales = 'free_x') + theme_bw() + 
  theme(
    axis.title.x = element_text(size = 30, face = 'bold'),
    axis.text.x = element_text(size = 20, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 30, face = 'bold'),
    legend.text = element_text(size = 25),
    legend.title = element_text(size = 30, face = 'bold'),
    strip.text.x = element_text(size = 15, face = 'bold'),
    strip.text.y = element_text(size = 20, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  xlab('')  + ylab('allele proportion (%)') +
  guides(color=guide_legend(title="correctness", reverse = TRUE))

#save
# ggsave("~/Freiburg_lab/ONT/cgmlst/figures/all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot.jpeg", all_summarized_dfs_edited_by_ST_duplicate_datapoints_noerrorbars_plot, device = "jpeg", width = 10000, height = 6000, units = 'px', dpi = 600)
