#  sub-sampling step, uncorrected sberry of uncorrected flye

# read barcode01
barcode01_subsampling_usuf <- readxl::read_excel('~/analysis/ONT_data/cgmlst/13012022/subsampled_05052022/transposed_cgmlst_uncorrected_sberry_uncorrected_flye_subsampled_05052022/barcode01_subs30x11mb_cgmlst.xlsx')

# barcode03
barcode03_subsampling_usuf <- readxl::read_excel('~/analysis/ONT_data/cgmlst/13012022/subsampled_05052022/transposed_cgmlst_uncorrected_sberry_uncorrected_flye_subsampled_05052022/barcode03_subs30x11mb_cgmlst.xlsx')

# barcode05
barcode05_subsampling_usuf <- readxl::read_excel('~/analysis/ONT_data/cgmlst/13012022/subsampled_05052022/transposed_cgmlst_uncorrected_sberry_uncorrected_flye_subsampled_05052022/barcode05_subs30x11mb_cgmlst.xlsx')

# barcode07
barcode07_subsampling_usuf <- readxl::read_excel('~/analysis/ONT_data/cgmlst/13012022/subsampled_05052022/transposed_cgmlst_uncorrected_sberry_uncorrected_flye_subsampled_05052022/barcode07_subs30x12mb_cgmlst.xlsx')

# barcode09
barcode09_subsampling_usuf <- readxl::read_excel('~/analysis/ONT_data/cgmlst/13012022/subsampled_05052022/transposed_cgmlst_uncorrected_sberry_uncorrected_flye_subsampled_05052022/barcode09_subs30x12mb_cgmlst.xlsx')


b1_subsampling_usuf_df <- single_double_allele_correctness(df = barcode01_subsampling_usuf, barcode_namee = "50:50", sberry_splitA = `A`, sberry_splitB = `B`, Illumina1 = `Kpn01412-210610`, Illumina2 = `Kpn01886-180907`) %>% mutate(Species = 'Klebsiella pneumoniae')

b3_subsampling_usuf_df <- single_double_allele_correctness(df = barcode03_subsampling_usuf, barcode_namee = "40:60", sberry_splitA = `A`, sberry_splitB = `B`, Illumina1 = `Kpn01528-180907`, Illumina2 = `Kpn01886-180907`)  %>% mutate(Species = 'Klebsiella pneumoniae')

b5_subsampling_usuf_df <- single_double_allele_correctness(df = barcode05_subsampling_usuf, barcode_namee = "30:70", sberry_splitA = `A`, sberry_splitB = `B`, Illumina1 = `13733-200204`, Illumina2 = `16902-200122`) %>% mutate(Species = 'Klebsiella pneumoniae')

b7_subsampling_usuf_df <- single_double_allele_correctness(df = barcode07_subsampling_usuf, barcode_namee = "20:30:50", sberry_splitA = `A`, sberry_splitB = `B`, Illumina1 = `15240-200224`, Illumina2 = `16126-200206`, Illumina3 = `5925-200127`, three_assemblies = TRUE) %>% mutate(Species = 'Klebsiella pneumoniae')

b9_subsampling_usuf_df <- single_double_allele_correctness(df = barcode09_subsampling_usuf, barcode_namee = "10:25:65", sberry_splitA = `A`, sberry_splitB = `B`, Illumina1 = `12926-200130`, Illumina2 = `13723-200204`, Illumina3 = `17053-200121`, three_assemblies = TRUE) %>% mutate(Species = 'Klebsiella pneumoniae')


b1b3b5b7b9_usuf_df <- b1_subsampling_usuf_df %>% bind_rows(b3_subsampling_usuf_df) %>% bind_rows(b5_subsampling_usuf_df) %>% 
  bind_rows(b7_subsampling_usuf_df) %>% bind_rows(b9_subsampling_usuf_df) %>%  
  mutate(double_single_correctness = case_when(double_single_correctness == 'double_alleles_correctly_called' ~ 'double allele correct',
                                               double_single_correctness == 'single_allele_correctly_called' ~ 'single allele correct',
                                               double_single_correctness == 'double_alleles_not_correctly_called' ~ 'double allele incorrect',
                                               double_single_correctness == 'single_allele_not_correctly_called' ~ 'single allele incorrect',
                                               double_single_correctness == 'no_allele_called_in_ONT_assembly' ~ 'no allele (ONT)',
                                               double_single_correctness == 'no_allele_called_in_Illumina_assemblies' ~ 'no allele (Illumina)',
                                               double_single_correctness == 'no_allele_called_in_ONT_and_Illumina_assemblies' ~ 'no allele (ONT and Illumina)')) 


# Based on Sandra's correction of my presentation on the 29th of April, 2022
# first check the id having the double_single_correctness == no_allele_called_in_ONT_and_Illumina_assemblies

b1b3b5b7b9_usuf_df %>% distinct(barcode_name, Species)
b1b3b5b7b9_usuf_df %>% filter(double_single_correctness == 'no allele (ONT)')
# Here, I see that b3, b7, b9 dfs were picked

# start

b1_subsampling_usuf_df_edited <- remove_rows_and_edit_df(b1_subsampling_usuf_df)

b3_subsampling_usuf_df_edited <- remove_rows_and_edit_df(b3_subsampling_usuf_df, no_allele_called_in_ONT_assembly = TRUE)

b5_subsampling_usuf_df_edited <- remove_rows_and_edit_df(b5_subsampling_usuf_df)

b7_subsampling_usuf_df_edited <- remove_rows_and_edit_df(b7_subsampling_usuf_df, no_allele_called_in_ONT_assembly = TRUE)

b9_subsampling_usuf_df_edited <- remove_rows_and_edit_df(b9_subsampling_usuf_df, no_allele_called_in_ONT_assembly = TRUE)

# bind edited
b1b3b5b7b9_usuf_df_edited <- b1_subsampling_usuf_df_edited %>% bind_rows(b3_subsampling_usuf_df_edited) %>% bind_rows(b5_subsampling_usuf_df_edited) %>% 
  bind_rows(b7_subsampling_usuf_df_edited) %>% bind_rows(b9_subsampling_usuf_df_edited) %>%  
  mutate(double_single_correctness = case_when(double_single_correctness == 'double_alleles_correctly_called' ~ 'double allele correct',
                                               double_single_correctness == 'single_allele_correctly_called' ~ 'single allele correct',
                                               double_single_correctness == 'double_alleles_not_correctly_called' ~ 'double allele incorrect',
                                               double_single_correctness == 'single_allele_not_correctly_called' ~ 'single allele incorrect',
                                               double_single_correctness == 'no_allele_called_in_ONT_assembly' ~ 'no allele (ONT)',
                                               double_single_correctness == 'no_allele_called_in_Illumina_assemblies' ~ 'no allele (Illumina)',
                                               double_single_correctness == 'no_allele_called_in_ONT_and_Illumina_assemblies' ~ 'no allele (ONT and Illumina)')) 


b1b3b5b7b9_usuf_df_edited_plot <- b1b3b5b7b9_usuf_df_edited %>% 
  ggplot(aes(x = factor(barcode_name, levels = c('50:50', '40:60', '30:70', '20:30:50', '10:25:65')), y = allele_proportion, fill = factor(double_single_correctness, levels = level_b1_b6))) +
  geom_col() +
  # geom_text(aes(label = if_else(number > 15, number, NULL)),
  #           position = position_stack(vjust = .5),  size=6, fontface = 'bold') +
  facet_grid(~ factor(Species,  levels = c('Klebsiella pneumoniae', 'Escherichia coli')), scales = 'free_x') +
  theme_bw() + ylab('Allele Proportion (%)') +
  scale_fill_manual(values = c('no allele (ONT)' = '#969696', 'single allele incorrect' = '#fdae6b', 'double allele incorrect' = '#d94801', 'single allele correct' = '#6baed6',  'double allele correct' = '#08519c')) +
  
  theme(
    axis.title.x = element_text(size = 40, face = 'bold'),
    axis.text.x = element_text(size = 35, face = 'bold', angle = 45, hjust = 1),
    axis.title.y = element_text(size = 40, face = 'bold'),
    legend.text = element_text(size = 30),
    legend.title = element_text(size = 35, face = 'bold'),
    strip.text.x = element_text(size = 35, face = 'bold'),
    strip.background = element_rect(colour="black",
                                    fill="white"),
    legend.position="right",
    axis.text.y = element_text(size = 35, face = 'bold')) +
  #scale_fill_discrete(limits = c('yes_Illumina_1&2&3', 'yes_Illumina_1&2', 'yes_Illumina_1&3', 'yes_Illumina_2&3', 'yes_Illumina_1', 'yes_Illumina_2', 'yes_Illumina_3', 'allele_not_called_in_barcode', 'allele_not_called_in_Illumina', 'no')) +
  #ggeasy::easy_rotate_x_labels(angle = 45, hjust = 1) + 
  xlab('')  +
  guides(fill=guide_legend(title="correctness", reverse = TRUE))
