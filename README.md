# ONT_first_trial


## Introduction
We aimed to identify potential cgmlst genes/alleles that can be used in strain resolution from a microbial community. We compared the core genome MLST profiles of [flye](https://github.com/fenderglass/Flye), [strainberry](https://github.com/rvicedomini/strainberry), and Illumina assemblies to identify potential 'marker' cgmlst loci.

## Authors and acknowledgment
Dr. Sandra Reuter and Dr. Hiren Ghosh contributed to this project.

## Project status
This project is still ongoing

