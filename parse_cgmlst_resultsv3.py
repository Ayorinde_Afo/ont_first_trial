import os
import pandas as pd
#import glob
#from functools import reduce
#import numpy as np
import sys

# the content of the directory has to end with .txt
if len(sys.argv) < 3:
    print("Incorrect usage.\nUsage:\npython3 parse_cgmlst_results.py /path/to/dir/containing/individual_cgmlst_results /path/to/output_dir/")
    sys.exit()
results_dir = sys.argv[1]
output_dir = sys.argv[2]
# filepath in glob.iglob("/mnt/c/Users/Afolayan/Documents/Freiburg_lab/ONT/cgmlst/13012022/cgmlst_from_error_correction_of_error_corrected_sberrz_assemblies_using_fastqs_derived_from_bam/*.txt"):


for file in os.listdir(results_dir):
    if file.endswith(".txt"):
        #print(file)
        filepath = os.path.join(results_dir, file)
        data = pd.read_csv(filepath, delim_whitespace=True , header = None, index_col = 0)
        data = data.transpose()
        # essentially if the number of columns is greater than 2. I may have to change this if the length is greater than 3 though.
        if 2 < len(data.columns) < 4:
            # replace symbols, 2nd column
            data.iloc[:, 1] = data.iloc[:, 1].replace(r"\?", r"", regex = True)
            data.iloc[:, 1] = data.iloc[:, 1].replace(r"\~", r"", regex = True)
            # replace symbols, 3rd column
            data.iloc[:, 2] = data.iloc[:, 2].replace(r"\?", r"", regex = True)
            data.iloc[:, 2] = data.iloc[:, 2].replace(r"\~", r"", regex = True)
            data = data[data.iloc[:, 0].str.contains("klebs") == False]
            # print(data.tail(6))
            # split column 2 by commas
            data = data.join(data.iloc[:, 1].str.split(',', expand=True).rename(columns={0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F'}))
            # split column 3 by commas
            data = data.join(data.iloc[:, 2].str.split(',', expand=True).rename(columns={0:'G', 1:'H', 2:'I', 3:'J'}))
            data = data.fillna('-')
        elif len(data.columns) >= 4:
            # replace symbols, 2nd column
            data.iloc[:, 1] = data.iloc[:, 1].replace(r"\?", r"", regex = True)
            data.iloc[:, 1] = data.iloc[:, 1].replace(r"\~", r"", regex = True)
            # replace symbols, 3rd column
            data.iloc[:, 2] = data.iloc[:, 2].replace(r"\?", r"", regex = True)
            data.iloc[:, 2] = data.iloc[:, 2].replace(r"\~", r"", regex = True)
            # replace symbols, 4th column
            data.iloc[:, 3] = data.iloc[:, 3].replace(r"\?", r"", regex = True)
            data.iloc[:, 3] = data.iloc[:, 3].replace(r"\~", r"", regex = True)
            data = data[data.iloc[:, 0].str.contains("klebs") == False]
            # split column 2 by commas
            data = data.join(data.iloc[:, 1].str.split(',', expand=True).rename(columns={0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F'}))
            # split column 3 by commas
            data = data.join(data.iloc[:, 2].str.split(',', expand=True).rename(columns={0:'G', 1:'H', 2:'I', 3:'J'}))
            # split column 4 by commas
            data = data.join(data.iloc[:, 3].str.split(',', expand=True).rename(columns={0:'K', 1:'L', 2:'M', 3:'P'}))
            data = data.fillna('-')
        elif data.iloc[:, 1].str.contains("\,").any():
            # that is, if the second column contains a comma: or df['Test'].str.contains('\,') or data.iloc[:, 1].str.contains("\,").any() or "," in data.iloc[:, 1].values
            # replace symbols, 2nd column
            data.iloc[:, 1] = data.iloc[:, 1].replace(r"\?", r"", regex = True)
            data.iloc[:, 1] = data.iloc[:, 1].replace(r"\~", r"", regex = True)
            data = data[data.iloc[:, 0].str.contains("klebs") == False]
            # print(data.tail(6))
            # split column 2 by commas
            data = data.join(data.iloc[:, 1].str.split(',', expand=True).rename(columns={0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F'}))
            data = data.fillna('-')
        else:
            # replace symbols, 2nd column
            data.iloc[:, 1] = data.iloc[:, 1].replace(r"\?", r"", regex = True)
            data.iloc[:, 1] = data.iloc[:, 1].replace(r"\~", r"", regex = True)
            data = data[data.iloc[:, 0].str.contains("klebs") == False]
            # print(data.tail(6))
            # split column 2 by commas
            data = data.join(data.iloc[:, 1].str.split(',', expand=True).rename(columns={0:'A', 1:'B', 2:'C', 3:'D', 4:'E', 5:'F'}))
            data = data.fillna('-')
        filename = os.path.basename(filepath)
        filename = os.path.splitext(filename)[0]
        #print(filename)
        filename = filename + ".csv"
        dirname = os.path.dirname(filepath)
        dirname = os.path.dirname(dirname)
        #print(dirname)
        #subdirname = filename.split("_")[0]
        outputdir = output_dir
        if not os.path.exists(outputdir):
            os.makedirs(outputdir)

        outputpath = os.path.join(outputdir, filename)
        data.to_csv(outputpath, index = False)
    else:
        print("error: check file extension or file itself")
