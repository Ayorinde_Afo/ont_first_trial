---
title: "Comparison of CGMLST Profiles Between Different Genome Assemblies"
subtitle: "Assemblies: Illumina, Flye, and Strainberry"
author: "Ayorinde Afolayan"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  html_document:
    df_print: kable
    toc: yes
  pdf_document:
    df_print:kable
  word_document: default
mainfont: Arial
fontsize: 12pt
---


```{r setup, include=FALSE}
library(tidyverse)
library(ggthemes)
library(magrittr)
library(kableExtra)
library(knitr)
library(scales)
source("../rmd_functions.R")

barcode03_0_5_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-5_cgmlst_all.xlsx', sheet = 'symboless')
barcode03_0_10_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-10_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_15_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-15_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_20_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-20_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_25_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-25_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_30_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-30_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_35_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-35_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_40_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-40_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_45_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-45_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_50_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-50_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_55_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-55_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_60_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-60_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_65_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-65_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_70_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-70_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_75_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-75_cgmlst.xlsx', sheet = 'symboless')
barcode03_0_78_split_remove <- readxl::read_xlsx('~/analysis/ONT_data/cgmlst/13012022/barcode03/barcode03_0-78_cgmlst.xlsx', sheet = 'symboless')

summary_file_barcode03 <- readxl::read_excel('~/analysis/ONT_data/cgmlst/13012022/barcode03/summary_barcode03.xlsx')
column_b3 <- c('SCHEME', 'ST', 'scgST')

knitr::opts_chunk$set(echo = TRUE)
```

### Introduction

---

We aimed to identify the minimum sequencing timepoint and sequence read size that is likely to be good enough for the differentiation of low-complexity metagenomes into strain level. 

#### Methodology

Raw metagenomic sequence files were combined in batches of 5, trimmed, assembled, and passed through the strainberry pipeline for strain resolution. The core genome MLST profiles of these assemblies were determined with the use of the [cgmlst tool](https://github.com/aldertzomer/cgmlst). The CGMLST profiles generated from flye and strainberry assemblies (from batches of raw metagenomic sequence data) were compared with the CGMLST profiles of known constituents/bacterial strains of the microbial community.

#### Figure 1: barcode03 - 40:60 proportion of _Klebsiella pneumoniae_ ST258 and ST307 genomes constitute the mock community. 

```{r warning=FALSE}
b3_0_5 <- data_for_two_assembliesv2(barcode03_0_5_split_remove, `barcode03_0-5_flye`, `barcode03_0-5_sberry`, `barcode03_0-5_sberryA`, `barcode03_0-5_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_5_columns <- b3_0_5 %>% select(8:ncol(.)) %>% names()
b3_0_5_count <- count_comparisons2(b3_0_5, b3_0_5_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_5_plot <- plot_comparisons4(b3_0_5_count, name_of_barcode = 'barcode03_0-5', two_assemblies = TRUE)
b3_0_5_plot

```

```{r warning=FALSE}
b3_0_10 <- data_for_two_assembliesv2(barcode03_0_10_split_remove, `barcode03_0-10_flye`, `barcode03_0-10_sberry`, `barcode03_0-10_sberryA`, `barcode03_0-10_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_10_columns <- b3_0_10 %>% select(8:ncol(.)) %>% names()
b3_0_10_count <- count_comparisons2(b3_0_10, b3_0_10_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_10_plot <- plot_comparisons4(b3_0_10_count, name_of_barcode = 'barcode03_0-10', two_assemblies = TRUE)
b3_0_10_plot

```

```{r warning=FALSE}
b3_0_15 <- data_for_two_assembliesv2(barcode03_0_15_split_remove, `barcode03_0-15_flye`, `barcode03_0-15_sberry`, `barcode03_0-15_sberryA`, `barcode03_0-15_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_15_columns <- b3_0_15 %>% select(8:ncol(.)) %>% names()
b3_0_15_count <- count_comparisons2(b3_0_15, b3_0_15_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_15_plot <- plot_comparisons4(b3_0_15_count, name_of_barcode = 'barcode03_0-15', two_assemblies = TRUE)
b3_0_15_plot

```

```{r warning=FALSE}
b3_0_20 <- data_for_two_assembliesv2(barcode03_0_20_split_remove, `barcode03_0-20_flye`, `barcode03_0-20_sberry`, `barcode03_0-20_sberryA`, `barcode03_0-20_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_20_columns <- b3_0_20 %>% select(8:ncol(.)) %>% names()
b3_0_20_count <- count_comparisons2(b3_0_20, b3_0_20_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_20_plot <- plot_comparisons4(b3_0_20_count, name_of_barcode = 'barcode03_0-20', two_assemblies = TRUE)
b3_0_20_plot

```

```{r warning=FALSE}
b3_0_25 <- data_for_two_assembliesv2(barcode03_0_25_split_remove, `barcode03_0-25_flye`, `barcode03_0-25_sberry`, `barcode03_0-25_sberryA`, `barcode03_0-25_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_25_columns <- b3_0_25 %>% select(8:ncol(.)) %>% names()
b3_0_25_count <- count_comparisons2(b3_0_25, b3_0_25_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_25_plot <- plot_comparisons4(b3_0_25_count, name_of_barcode = 'barcode03_0-25', two_assemblies = TRUE)
b3_0_25_plot

```

```{r warning=FALSE}
b3_0_30 <- data_for_two_assembliesv2(barcode03_0_30_split_remove, `barcode03_0-30_flye`, `barcode03_0-30_sberry`, `barcode03_0-30_sberryA`, `barcode03_0-30_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_30_columns <- b3_0_30 %>% select(8:ncol(.)) %>% names()
b3_0_30_count <- count_comparisons2(b3_0_30, b3_0_30_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_30_plot <- plot_comparisons4(b3_0_30_count, name_of_barcode = 'barcode03_0-30', two_assemblies = TRUE)
b3_0_30_plot

```


```{r warning=FALSE}
b3_0_35 <- data_for_two_assembliesv2(barcode03_0_35_split_remove, `barcode03_0-35_flye`, `barcode03_0-35_sberry`, `barcode03_0-35_sberryA`, `barcode03_0-35_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_35_columns <- b3_0_35 %>% select(8:ncol(.)) %>% names()
b3_0_35_count <- count_comparisons2(b3_0_35, b3_0_35_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_35_plot <- plot_comparisons4(b3_0_35_count, name_of_barcode = 'barcode03_0-35', two_assemblies = TRUE)
b3_0_35_plot

```

```{r warning=FALSE}
b3_0_40 <- data_for_two_assembliesv2(barcode03_0_40_split_remove, `barcode03_0-40_flye`, `barcode03_0-40_sberry`, `barcode03_0-40_sberryA`, `barcode03_0-40_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_40_columns <- b3_0_40 %>% select(8:ncol(.)) %>% names()
b3_0_40_count <- count_comparisons2(b3_0_40, b3_0_40_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_40_plot <- plot_comparisons4(b3_0_40_count, name_of_barcode = 'barcode03_0-40', two_assemblies = TRUE)
b3_0_40_plot

```

```{r warning=FALSE}
b3_0_45 <- data_for_two_assembliesv2(barcode03_0_45_split_remove, `barcode03_0-45_flye`, `barcode03_0-45_sberry`, `barcode03_0-45_sberryA`, `barcode03_0-45_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_45_columns <- b3_0_45 %>% select(8:ncol(.)) %>% names()
b3_0_45_count <- count_comparisons2(b3_0_45, b3_0_45_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_45_plot <- plot_comparisons4(b3_0_45_count, name_of_barcode = 'barcode03_0-45', two_assemblies = TRUE)
b3_0_45_plot

```


```{r warning=FALSE}
b3_0_50 <- data_for_two_assembliesv2(barcode03_0_50_split_remove, `barcode03_0-50_flye`, `barcode03_0-50_sberry`, `barcode03_0-50_sberryA`, `barcode03_0-50_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_50_columns <- b3_0_50 %>% select(8:ncol(.)) %>% names()
b3_0_50_count <- count_comparisons2(b3_0_50, b3_0_50_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_50_plot <- plot_comparisons4(b3_0_50_count, name_of_barcode = 'barcode03_0-50', two_assemblies = TRUE)
b3_0_50_plot

```


```{r warning=FALSE}
b3_0_55 <- data_for_two_assembliesv2(barcode03_0_55_split_remove, `barcode03_0-55_flye`, `barcode03_0-55_sberry`, `barcode03_0-55_sberryA`, `barcode03_0-55_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_55_columns <- b3_0_55 %>% select(8:ncol(.)) %>% names()
b3_0_55_count <- count_comparisons2(b3_0_55, b3_0_55_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_55_plot <- plot_comparisons4(b3_0_55_count, name_of_barcode = 'barcode03_0-55', two_assemblies = TRUE)
b3_0_55_plot

```


```{r warning=FALSE}
b3_0_60 <- data_for_two_assembliesv2(barcode03_0_60_split_remove, `barcode03_0-60_flye`, `barcode03_0-60_sberry`, `barcode03_0-60_sberryA`, `barcode03_0-60_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_60_columns <- b3_0_60 %>% select(8:ncol(.)) %>% names()
b3_0_60_count <- count_comparisons2(b3_0_60, b3_0_60_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_60_plot <- plot_comparisons4(b3_0_60_count, name_of_barcode = 'barcode03_0-60', two_assemblies = TRUE)
b3_0_60_plot

```

```{r warning=FALSE}
b3_0_65 <- data_for_two_assembliesv2(barcode03_0_65_split_remove, `barcode03_0-65_flye`, `barcode03_0-65_sberry`, `barcode03_0-65_sberryA`, `barcode03_0-65_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_65_columns <- b3_0_65 %>% select(8:ncol(.)) %>% names()
b3_0_65_count <- count_comparisons2(b3_0_65, b3_0_65_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_65_plot <- plot_comparisons4(b3_0_65_count, name_of_barcode = 'barcode03_0-65', two_assemblies = TRUE)
b3_0_65_plot

```

```{r warning=FALSE}
b3_0_70 <- data_for_two_assembliesv2(barcode03_0_70_split_remove, `barcode03_0-70_flye`, `barcode03_0-70_sberry`, `barcode03_0-70_sberryA`, `barcode03_0-70_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_70_columns <- b3_0_70 %>% select(8:ncol(.)) %>% names()
b3_0_70_count <- count_comparisons2(b3_0_70, b3_0_70_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_70_plot <- plot_comparisons4(b3_0_70_count, name_of_barcode = 'barcode03_0-70', two_assemblies = TRUE)
b3_0_70_plot

```

```{r warning=FALSE}
b3_0_75 <- data_for_two_assembliesv2(barcode03_0_75_split_remove, `barcode03_0-75_flye`, `barcode03_0-75_sberry`, `barcode03_0-75_sberryA`, `barcode03_0-75_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_75_columns <- b3_0_75 %>% select(8:ncol(.)) %>% names()
b3_0_75_count <- count_comparisons2(b3_0_75, b3_0_75_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_75_plot <- plot_comparisons4(b3_0_75_count, name_of_barcode = 'barcode03_0-75', two_assemblies = TRUE)
b3_0_75_plot

```

```{r warning=FALSE}
b3_0_78 <- data_for_two_assembliesv2(barcode03_0_78_split_remove, `barcode03_0-78_flye`, `barcode03_0-78_sberry`, `barcode03_0-78_sberryA`, `barcode03_0-78_sberryB`, `Kpn01528-180907`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b3_0_78_columns <- b3_0_78 %>% select(8:ncol(.)) %>% names()
b3_0_78_count <- count_comparisons2(b3_0_78, b3_0_78_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b3_0_78_plot <- plot_comparisons4(b3_0_78_count, name_of_barcode = 'barcode03_0-78', two_assemblies = TRUE)
b3_0_78_plot

```

#### Summary

The following table summarizes the differences between the degree of strain resolution observed using different arithmetic batches of low-complexity metagenomic datasets.

```{r  echo=FALSE, warning=FALSE}
summary_file_barcode03 %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```

#### Plot of summary

```{r  echo=FALSE, warning=FALSE}
barcode03_timeplot <- summary_file_barcode03 %>% pivot_longer(-Content, names_to = "Tool", values_to = "proportion_undetected") %>% filter(!Tool %in% c("Number of CGMLST loci not detected by Flye", "Number of CGMLST loci not detected by Strainberry")) %>% 
  mutate(Tool = if_else(Tool == "Proportion of CGMLST loci not detected by Strainberry", "Strainberry", "Flye")) %>% 
  ggplot(aes(x = Content, y = proportion_undetected, group = Tool, colour = Tool)) + geom_line() + geom_point() + scale_color_manual(values=c('#999999','#E69F00')) +
  theme_minimal() +
  ggeasy::easy_rotate_x_labels(angle = 300) + scale_x_discrete(limits = c("barcode03_0-5", "barcode03_0-10", "barcode03_0-15", "barcode03_0-20",
                                                                          "barcode03_0-25", "barcode03_0-30", "barcode03_0-35", "barcode03_0-40",
                                                                          "barcode03_0-45", "barcode03_0-50", "barcode03_0-55", "barcode03_0-60", 
                                                                          "barcode03_0-65", "barcode03_0-70", "barcode03_0-75", "barcode03_0-78")) +
  ylab("Proportion of undetected cgMLST loci (%)") + ggtitle(expression(atop(paste(bold("Barcode03: "), "40:60 proportion of", italic(" Klebsiella pneumoniae"), " ST258 and ST307"))))

# view
barcode03_timeplot
```
