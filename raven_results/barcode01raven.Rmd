---
title: "Comparison of CGMLST Profiles Between Different Genome Assemblies"
subtitle: "Assemblies: Illumina, Raven, and Strainberry"
author: "Ayorinde Afolayan"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  html_document:
    df_print: kable
    toc: yes
  pdf_document:
    df_print:kable
  word_document: default
mainfont: Arial
fontsize: 12pt
---


```{r setup, include=FALSE}
library(tidyverse)
library(ggthemes)
library(magrittr)
library(kableExtra)
library(knitr)
library(scales)

source("../rmd_functions.R")
List_of_file_pathsb01 <- list.files(path ="~/analysis/ONT_data/cgmlst/merged_cgmlst/barcode01/", pattern = ".csv", all.files = TRUE, full.names = TRUE)

barcode01_0_5 <- readr::read_delim(List_of_file_pathsb01[9], delim = ';')
barcode01_0_10 <- readr::read_csv(List_of_file_pathsb01[1])
barcode01_0_15 <- readr::read_csv(List_of_file_pathsb01[2])
barcode01_0_20 <- readr::read_csv(List_of_file_pathsb01[3])
barcode01_0_25 <- readr::read_csv(List_of_file_pathsb01[4])
barcode01_0_30 <- readr::read_csv(List_of_file_pathsb01[5])
barcode01_0_35 <- readr::read_csv(List_of_file_pathsb01[6])
barcode01_0_40 <- readr::read_csv(List_of_file_pathsb01[7])
barcode01_0_45 <- readr::read_csv(List_of_file_pathsb01[8])
barcode01_0_50 <- readr::read_csv(List_of_file_pathsb01[10])
barcode01_0_55 <- readr::read_csv(List_of_file_pathsb01[11])
barcode01_0_60 <- readr::read_csv(List_of_file_pathsb01[12])
barcode01_0_62 <- readr::read_csv(List_of_file_pathsb01[13])

summary_file_barcode01 <- readxl::read_excel('~/analysis/ONT_data/cgmlst/13012022/barcode01/13012022_summary.xlsx')
column_b3 <- c('SCHEME', 'ST', 'scgST')

knitr::opts_chunk$set(echo = TRUE)
```

### Introduction

---

We aimed to identify the minimum sequencing timepoint and sequence read size that is likely to be good enough for the differentiation of low-complexity metagenomes into strain level. 

#### Methodology

Raw metagenomic sequence files were combined in batches of 5, trimmed, assembled, and passed through the strainberry pipeline for strain resolution. The core genome MLST profiles of these assemblies were determined with the use of the [cgmlst tool](https://github.com/aldertzomer/cgmlst). The CGMLST profiles generated from flye and strainberry assemblies (from batches of raw metagenomic sequence data) were compared with the CGMLST profiles of known constituents/bacterial strains of the microbial community.

#### Figure 1: Barcode01 - Equal proportion of _Klebsiella pneumoniae_ ST258 and ST307 genomes constitute the mock community. 

```{r warning=FALSE}
b1_0_5 <- data_for_two_assembliesv2(barcode01_0_5, `barcode01_0-5_raven`, `barcode01_0-5_sberry`, `barcode01_0-5_sberryA`, `barcode01_0-5_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_5_columns <- b1_0_5 %>% select(8:ncol(.)) %>% names()
b1_0_5_count <- count_comparisons2(b1_0_5, b1_0_5_columns)
#write.table(b1_0_5_count, '../../ont_first_trial/sum_counts/raven_sberry/barcode01/barcode01_0-05.csv', sep = ',', row.names = FALSE)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_5_plot <- plot_comparisons4(b1_0_5_count, name_of_barcode = 'barcode01_0-5', two_assemblies = TRUE)
b1_0_5_plot

```

```{r warning=FALSE}
b1_0_10 <- data_for_two_assembliesv2(barcode01_0_10, `barcode01_0-10_raven`, `barcode01_0-10_sberry`, `barcode01_0-10_sberryA`, `barcode01_0-10_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_10_columns <- b1_0_10 %>% select(8:ncol(.)) %>% names()
b1_0_10_count <- count_comparisons2(b1_0_10, b1_0_10_columns)
#write.table(b1_0_10_count, '../../ont_first_trial/sum_counts/raven_sberry/barcode01/barcode01_0-10.csv', sep = ',', row.names = FALSE)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_10_plot <- plot_comparisons4(b1_0_10_count, name_of_barcode = 'barcode01_0-10', two_assemblies = TRUE)
b1_0_10_plot

```

```{r warning=FALSE}
b1_0_15 <- data_for_two_assembliesv2(barcode01_0_15, `barcode01_0-15_raven`, `barcode01_0-15_sberry`, `barcode01_0-15_sberryA`, `barcode01_0-15_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_15_columns <- b1_0_15 %>% select(8:ncol(.)) %>% names()
b1_0_15_count <- count_comparisons2(b1_0_15, b1_0_15_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_15_plot <- plot_comparisons4(b1_0_15_count, name_of_barcode = 'barcode01_0-15', two_assemblies = TRUE)
b1_0_15_plot

```

```{r warning=FALSE}
b1_0_20 <- data_for_two_assembliesv2(barcode01_0_20, `barcode01_0-20_raven`, `barcode01_0-20_sberry`, `barcode01_0-20_sberryA`, `barcode01_0-20_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_20_columns <- b1_0_20 %>% select(8:ncol(.)) %>% names()
b1_0_20_count <- count_comparisons2(b1_0_20, b1_0_20_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_20_plot <- plot_comparisons4(b1_0_20_count, name_of_barcode = 'barcode01_0-20', two_assemblies = TRUE)
b1_0_20_plot

```

```{r warning=FALSE}
b1_0_25 <- data_for_two_assembliesv2(barcode01_0_25, `barcode01_0-25_raven`, `barcode01_0-25_sberry`, `barcode01_0-25_sberryA`, `barcode01_0-25_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_25_columns <- b1_0_25 %>% select(8:ncol(.)) %>% names()
b1_0_25_count <- count_comparisons2(b1_0_25, b1_0_25_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_25_plot <- plot_comparisons4(b1_0_25_count, name_of_barcode = 'barcode01_0-25', two_assemblies = TRUE)
b1_0_25_plot

```

```{r warning=FALSE}
b1_0_30 <- data_for_two_assembliesv2(barcode01_0_30, `barcode01_0-30_raven`, `barcode01_0-30_sberry`, `barcode01_0-30_sberryA`, `barcode01_0-30_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_30_columns <- b1_0_30 %>% select(8:ncol(.)) %>% names()
b1_0_30_count <- count_comparisons2(b1_0_30, b1_0_30_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_30_plot <- plot_comparisons4(b1_0_30_count, name_of_barcode = 'barcode01_0-30', two_assemblies = TRUE)
b1_0_30_plot

```


```{r warning=FALSE}
b1_0_35 <- data_for_two_assembliesv2(barcode01_0_35, `barcode01_0-35_raven`, `barcode01_0-35_sberry`, `barcode01_0-35_sberryA`, `barcode01_0-35_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_35_columns <- b1_0_35 %>% select(8:ncol(.)) %>% names()
b1_0_35_count <- count_comparisons2(b1_0_35, b1_0_35_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_35_plot <- plot_comparisons4(b1_0_35_count, name_of_barcode = 'barcode01_0-35', two_assemblies = TRUE)
b1_0_35_plot

```

```{r warning=FALSE}
b1_0_40 <- data_for_two_assembliesv2(barcode01_0_40, `barcode01_0-40_raven`, `barcode01_0-40_sberry`, `barcode01_0-40_sberryA`, `barcode01_0-40_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_40_columns <- b1_0_40 %>% select(8:ncol(.)) %>% names()
b1_0_40_count <- count_comparisons2(b1_0_40, b1_0_40_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_40_plot <- plot_comparisons4(b1_0_40_count, name_of_barcode = 'barcode01_0-40', two_assemblies = TRUE)
b1_0_40_plot

```

```{r warning=FALSE}
b1_0_45 <- data_for_two_assembliesv2(barcode01_0_45, `barcode01_0-45_raven`, `barcode01_0-45_sberry`, `barcode01_0-45_sberryA`, `barcode01_0-45_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_45_columns <- b1_0_45 %>% select(8:ncol(.)) %>% names()
b1_0_45_count <- count_comparisons2(b1_0_45, b1_0_45_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_45_plot <- plot_comparisons4(b1_0_45_count, name_of_barcode = 'barcode01_0-45', two_assemblies = TRUE)
b1_0_45_plot

```


```{r warning=FALSE}
b1_0_50 <- data_for_two_assembliesv2(barcode01_0_50, `barcode01_0-50_raven`, `barcode01_0-50_sberry`, `barcode01_0-50_sberryA`, `barcode01_0-50_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_50_columns <- b1_0_50 %>% select(8:ncol(.)) %>% names()
b1_0_50_count <- count_comparisons2(b1_0_50, b1_0_50_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_50_plot <- plot_comparisons4(b1_0_50_count, name_of_barcode = 'barcode01_0-50', two_assemblies = TRUE)
b1_0_50_plot

```


```{r warning=FALSE}
b1_0_55 <- data_for_two_assembliesv2(barcode01_0_55, `barcode01_0-55_raven`, `barcode01_0-55_sberry`, `barcode01_0-55_sberryA`, `barcode01_0-55_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_55_columns <- b1_0_55 %>% select(8:ncol(.)) %>% names()
b1_0_55_count <- count_comparisons2(b1_0_55, b1_0_55_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_55_plot <- plot_comparisons4(b1_0_55_count, name_of_barcode = 'barcode01_0-55', two_assemblies = TRUE)
b1_0_55_plot

```


```{r warning=FALSE}
b1_0_60 <- data_for_two_assembliesv2(barcode01_0_60, `barcode01_0-60_raven`, `barcode01_0-60_sberry`, `barcode01_0-60_sberryA`, `barcode01_0-60_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_60_columns <- b1_0_60 %>% select(8:ncol(.)) %>% names()
b1_0_60_count <- count_comparisons2(b1_0_60, b1_0_60_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_60_plot <- plot_comparisons4(b1_0_60_count, name_of_barcode = 'barcode01_0-60', two_assemblies = TRUE)
b1_0_60_plot

```

```{r warning=FALSE}
b1_0_62 <- data_for_two_assembliesv2(barcode01_0_62, `barcode01_0-62_raven`, `barcode01_0-62_sberry`, `barcode01_0-62_sberryA`, `barcode01_0-62_sberryB`, `Kpn01412-210610`, `Kpn01886-180907`, exclude_columns = column_b3)

# count
b1_0_62_columns <- b1_0_62 %>% select(8:ncol(.)) %>% names()
b1_0_62_count <- count_comparisons2(b1_0_62, b1_0_62_columns)
# b1_splittv2_count %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
# plot
b1_0_62_plot <- plot_comparisons4(b1_0_62_count, name_of_barcode = 'barcode01_0-62', two_assemblies = TRUE)
b1_0_62_plot

```

#### Summary

The following table summarizes the differences between the degree of strain resolution observed using different arithmetic batches of low-complexity metagenomic datasets.

```{r  echo=FALSE, warning=FALSE}
summary_file %>% kable() %>% kable_styling() %>% scroll_box(width = "100%")
```

#### Plot of summary

```{r  echo=FALSE, warning=FALSE}
barcode01_timeplot <- summary_file_barcode01 %>% pivot_longer(-Content, names_to = "Tool", values_to = "proportion_undetected") %>% filter(!Tool %in% c("Number of CGMLST loci not detected by Flye", "Number of CGMLST loci not detected by Raven", "Number of CGMLST loci not detected by Strainberry")) %>% 
  mutate(Tool = case_when(Tool == "Proportion of CGMLST loci not detected by Strainberry" ~ "Strainberry",
                          Tool == "Proportion of CGMLST loci not detected by Flye" ~ "Flye",
                          Tool == "Proportion of CGMLST loci not detected by Raven" ~ "Raven",
                          TRUE ~ as.character(Content))) %>% 
 # mutate(Tool = if_else(Tool == "Proportion of CGMLST loci not detected by Strainberry", "Strainberry", "Flye")) %>% 
  ggplot(aes(x = Content, y = proportion_undetected, group = Tool, colour = Tool)) + geom_line() + geom_point() + scale_color_manual(values=c('#999999','#E69F00', 'blue')) +
  theme_minimal() +
  ggeasy::easy_rotate_x_labels(angle = 300) + scale_x_discrete(limits = c("barcode01_0-5", "barcode01_0-10", "barcode01_0-15", "barcode01_0-20",
                                                                          "barcode01_0-25", "barcode01_0-30", "barcode01_0-35", "barcode01_0-40",
                                                                          "barcode01_0-45", "barcode01_0-50", "barcode01_0-55", "barcode01_0-60", 
                                                                          "barcode01_0-62")) +
  ylab("Proportion of undetected cgMLST loci (%)") + ggtitle(expression(atop(paste(bold("Barcode01: "), "Equal proportion of", italic(" Klebsiella pneumoniae"), " ST258 and ST307"))))

# view
barcode01_timeplot
```
