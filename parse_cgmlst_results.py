import os
import pandas as pd
#import glob
#from functools import reduce
#import numpy as np
import sys

# the content of the directory has to end with .txt
if len(sys.argv) < 2:
    print("Incorrect usage.\nUsage:\npython3 parse_cgmlst_results.py /path/to/dir/containing/individual_cgmlst_results")
    sys.exit()
results_dir = sys.argv[1]
# filepath in glob.iglob("/mnt/c/Users/Afolayan/Documents/Freiburg_lab/ONT/cgmlst/13012022/cgmlst_from_error_correction_of_error_corrected_sberrz_assemblies_using_fastqs_derived_from_bam/*.txt"):


for file in os.listdir(results_dir):
    if file.endswith(".txt"):
        #print(file)
        filepath = os.path.join(results_dir, file)
        data = pd.read_csv(filepath, delim_whitespace=True , header = None, index_col = 0)
        data = data.transpose()
        data.iloc[:, 1] = data.iloc[:, 1].replace(r"\?", r"", regex = True)
        data.iloc[:, 1] = data.iloc[:, 1].replace(r"\~", r"", regex = True)
        data = data[data.iloc[:, 0].str.contains("klebs") == False]
        # print(data.tail(6))
        # split column 2 by commas
        data = data.join(data.iloc[:, 1].str.split(',', expand=True).rename(columns={0:'A', 1:'B', 2:'C'}))
        data = data.fillna('-')
        filename = os.path.basename(filepath)
        filename = os.path.splitext(filename)[0]
        #print(filename)
        filename = filename + ".csv"
        dirname = os.path.dirname(filepath)
        dirname = os.path.dirname(dirname)
        #print(dirname)
        #subdirname = filename.split("_")[0]
        outputdir = os.path.join(dirname, "cgmlst_transposed")
        if not os.path.exists(outputdir):
            os.makedirs(outputdir)
    
        outputpath = os.path.join(outputdir, filename)
        data.to_csv(outputpath, index = False)
    else:
        print("error: check file extension or file itself")    
    